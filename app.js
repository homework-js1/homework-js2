let userName;
let userAge;
let userAnswer;

userName = prompt("Enter your name!!");
while (userName === "" || !isNaN(userName) || !/^[a-zA-Z]+$/.test(userName)) {
    userName = prompt("Oops...Re-enter your name!!", userName);
}
userAge = +prompt("Enter your age!!!");
while (userAge === 0 || Number(isNaN(userAge))) {
    userAge = +prompt("Oops...Re-enter your age!!", userAge);
}

if (userAge < 18) {
    alert("You are not allowed to visit this website.");
} else if (userAge >= 18 && userAge <= 22) {
    userAnswer = confirm("Are you sure you want to continue?");
    if (userAnswer) {
        alert(`Welcome, ${userName}`);
    } else  {
        alert("You are not allowed to visit this website.");
    }
} else {
    alert(`Welcome, ${userName}`);
}
